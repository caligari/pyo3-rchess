use pyo3::prelude::*;

#[pymodule]
fn pyo3_rchess(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(get_fen, m)?)?;
    m.add_function(wrap_pyfunction!(pseudo_legals, m)?)?;
    Ok(())
}

#[derive(FromPyObject)]
struct PyBoard {
    occupied_co: Vec<u64>,
    pawns: u64,
    knights: u64,
    bishops: u64,
    rooks: u64,
    queens: u64,
    kings: u64,
}

#[pyfunction]
fn get_fen(b: PyBoard) -> PyResult<String> {
    Ok(rchess::fen::fen_board(&board_from_pyboard(b)))
}

#[pyfunction]
fn pseudo_legals(b: PyBoard, from_squares: u64, to_squares: u64) -> PyResult<Vec<(u8, u8)>> {
    let board = board_from_pyboard(b);
    let dst_mask = rchess::bitboard::Bitboard::from(to_squares);
    Ok(Vec::from_iter(
        rchess::bitboard::Bitboard::from(from_squares)
            .dump_squares()
            .iter()
            .flat_map(|&src| {
                (rchess::pseudo_legals::pseudo_legals(board, src) & dst_mask)
                    .dump_squares()
                    .iter()
                    .map(|&dst| (src, dst))
                    .collect::<Vec<_>>()
            }),
    ))
}

fn board_from_pyboard(b: PyBoard) -> rchess::board::Board {
    rchess::board::Board::from_bitboards(
        b.occupied_co[1],
        b.occupied_co[0],
        b.pawns,
        b.knights,
        b.bishops,
        b.rooks,
        b.queens,
        b.kings,
    )
}
