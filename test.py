
import chess
import pyo3_rchess

board = chess.Board()
fen = pyo3_rchess.get_fen(board)
print(f"{fen}")
print(",".join(map(str, board.generate_pseudo_legal_moves_rchess())))

board.push(chess.Move.from_uci("g1f3"))
fen = pyo3_rchess.get_fen(board)
print(f"{fen}")
print(",".join(map(str, board.generate_pseudo_legal_moves_rchess())))

board.push(chess.Move.from_uci("e7e5"))
fen = pyo3_rchess.get_fen(board)
print(f"{fen}")
print(",".join(map(str, board.generate_pseudo_legal_moves_rchess())))

